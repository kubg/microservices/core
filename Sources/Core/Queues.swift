import Vapor
import QueuesRedisDriver
import QueuesFluentDriver

extension ApplicationEntrypoint {
    var redisURL: URL? {
        guard let redisURL = Environment.process.REDIS_URL else {
            return nil
        }

        guard let url = URL(string: redisURL) else {
            return nil
        }
        
        return url
    }
    
    func configureQueues() async throws {
        switch environment {
        case .production:
            if let url = redisURL {
                try configureProductionQueues(url: url)
            } else {
                fatalError("REDIS_URL is not set")
            }
        case .development:
            if let url = redisURL {
                try configureProductionQueues(url: url)
            } else {
                try await configureDevelopmentQueues()
            }
        case .testing:
            configureTestingQueues()
        default:
            break
        }
    }
    
    private func configureProductionQueues(url: URL) throws {
        try application.queues.use(.redis(url: url, pool: .init(connectionRetryTimeout: .seconds(60))))
    }
    
    private func configureDevelopmentQueues() async throws {
        application.queues.use(.fluent(.sqlite))
        application.migrations.add(JobMetadataMigrate())
        try await application.autoMigrate()
        try application.queues.startInProcessJobs()
    }
    
    private func configureTestingQueues() {
        application.queues.use(.fluent(.sqlite))
    }
}

private extension Application.Queues.Provider {
    static func redis(url: URL, pool: RedisConfiguration.PoolOptions) throws -> Self {
        try .redis(.init(url: url, pool: pool))
    }
}
