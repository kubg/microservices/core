import Foundation
import Vapor

public final class ApplicationEntrypoint {
    public typealias ConfigureClosure = (Application) async throws -> Void
    
    var environment: Environment
    var application: Application
    
    var configure: ConfigureClosure
    var migrations: ConfigureClosure
    var jobs: ConfigureClosure
    var routes: ConfigureClosure
    
    public init(
        environment: Environment,
        configure: @escaping ConfigureClosure,
        migrations: @escaping ConfigureClosure,
        jobs: @escaping ConfigureClosure,
        routes: @escaping ConfigureClosure
    ) throws {
        self.environment = environment
        self.configure = configure
        self.migrations = migrations
        self.jobs = jobs
        self.routes = routes
        self.application = Application(environment)
        
        try boot(application)
    }
    
    private func boot(_ app: Application) throws {
        try LoggingSystem.bootstrap(from: &environment)
        app.logger.info("Application booted")
    }
    
    private func setup() async throws {
        try await configure(application)
        try await configureDatabase()
        try await migrations(application)
        try await configureQueues()
        try await jobs(application)
        try await routes(application)
    }
    
    public func run() async throws {
        defer { application.shutdown() }
        
        do {
            try await setup()
        } catch {
            application.logger.report(error: error)
            throw error
        }
        try await application.execute()
    }
}
