import Vapor
import Fluent
import FluentSQLiteDriver
import FluentPostgresDriver

internal extension ApplicationEntrypoint {
    func configureDatabase() async throws {
        switch environment {
        case .production:
            try configureProductionDatabase()
        case .development:
            try await configureDevelopmentDatabase()
        case .testing:
            configureTestingDatabase()
        default:
            break
        }
    }
    
    private func configureProductionDatabase() throws {
        guard let psqlURL = Environment.process.DATABASE_URL else {
            fatalError("No value was found at the given public key environment 'DATABASE_URL'")
        }

        guard let url = URL(string: psqlURL) else {
            fatalError("Cannot parse: \(psqlURL) correctly.")
        }
        
        application.databases.use(try .postgres(url: url), as: .psql)
    }
    
    private func configureDevelopmentDatabase() async throws {
        let configuration = SQLiteConfiguration(storage: .memory)
        application.databases.use(.sqlite(configuration), as: .sqlite)
        try await application.autoMigrate()
    }
    
    private func configureTestingDatabase() {
        let configuration = SQLiteConfiguration(storage: .memory)
        application.databases.use(.sqlite(configuration), as: .sqlite)
    }
}
